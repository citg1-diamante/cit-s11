package com.zuitt.discussion.models;

import javax.persistence.*;

@Entity
@Table(name = "posts")
public class Post {

    //Properties
    //indicates primary key
    @Id
    //auto increment
    @GeneratedValue
    private Long id;

    //Class properties that represents table column in a relational database are annotated as @Column
    @Column
    private String title;

    @Column
    private String content;

    //Constructors

    //Default contructors are required when retrieving data from the database or class
    public Post(){}

    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    //Getters and Setters
    public String getTitle(){
        return title;
    }

    public void setTitle(String Title){
        this.title=Title;
    }

    public String getContent(){
        return content;
    }

    public void setContent(String Content){
        this.content=Content;
    }

}
