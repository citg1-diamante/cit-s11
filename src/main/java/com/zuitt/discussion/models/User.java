package com.zuitt.discussion.models;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class User {

    //Properties
    //indicates primary key
    @Id
    //auto increment
    @GeneratedValue
    private Long userid;

    //Class properties that represents table column in a relational database are annotated as @Column
    @Column
    private String username;

    @Column
    private String password;

    //Constructors

    //Default contructors are required when retrieving data from the database or class
    public User(){}

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    //Getters and Setters
    public String getUsername(){
        return username;
    }

    public void setUsername(String Username){
        this.username = Username;
    }

    public String getPassword(){
        return password;
    }

    public void setPassword(String Password){
        this.password = Password;
    }

}
